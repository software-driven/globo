# Globo

software created in the course of driven

# Requisitos

- [X] Aplicar o layout seguindo o figma
- [X] Utilizar a fonte Encode Sans ná página. Como não e uma fonte padrão nos computadores, você terá que importá-la na página
- [X] Todas as notícias devem ser links. Não é necessario colocar links para as página reais

# Requisitos Bônus

- [X] A logo deve mudar para a logo mobile;
- [X] O menu de navegação deve ficar embaixo da logo
- [X] As notícias devem ficar empilhadas, conforme mosta o layout movel